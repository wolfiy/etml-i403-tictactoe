﻿/// ETML
/// Auteur:         Sébastien Tille
/// Date:           28.09.2023
/// Description:    Implémentation du jeu Tic Tac Toe en console.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ch.etml.TicTacToe
{
    internal class Program
    {
        private const int ROWS = 3;
        private const int COLUMNS = 3;

        private static bool isPlayerOne;
        private static bool hasWon;

        private static bool[,] positionIsPlayed = new bool[ROWS, COLUMNS];
        private static char[,] grid = new char[ROWS, COLUMNS];


        static void Main(string[] args)
        {
            Console.WriteLine(
                "===========\n" +
                "Tic Tac Toe\n" +
                "===========\n");

            isPlayerOne = true;
            hasWon = false;
            grid = InitializeGrid(ROWS, COLUMNS);
            DrawBoard(grid, ROWS, COLUMNS);

            while (!hasWon)
            {
                if (isPlayerOne) PlaceCharacter('x');
                else PlaceCharacter('o');
            }

            Console.WriteLine("TERMINE");
            
            Console.ReadLine();
        }

        private static char[,] InitializeGrid(int rows, int columns)
        {
            char[,] _grid = new char[ROWS, COLUMNS];
            
            for (int i = 0; i < ROWS; ++i)
            {
                for (int j = 0; j < COLUMNS; ++j)
                {
                    _grid[i, j] = ' ';
                }
            }

            return _grid;
        }

        private static void PlaceCharacter(char symbol)
        {
            int r = -1, c = -1;

            while(!PositionIsValid(r, c))
            {
                Console.Write("\nLigne (1~3): ");
                r = Convert.ToInt16(Console.ReadLine()) - 1;

                Console.Write("Colonne (1~3): ");
                c = Convert.ToInt16(Console.ReadLine()) - 1;
            }

            grid[r, c] = symbol;
            positionIsPlayed[r, c] = true;

            isPlayerOne = !isPlayerOne;
            DrawBoard(grid, ROWS, COLUMNS);
            IsWinning(grid, symbol);
        }

        private static bool PositionIsValid(int r, int c)
        {
            return (0 <= r) && (r < ROWS)
                && (0 <= c) && (c < COLUMNS)
                && !positionIsPlayed[r, c];
        }

        private static bool IsWinning(char[,] _grid, char c)
        {
            for (int i = 0; i < ROWS; ++i)
            {
                Console.WriteLine(hasWon);
                if ((_grid[0, i] == c && _grid[1, i] == c && _grid[2, i] == c)
                 || (_grid[i, 0] == c && _grid[i, 1] == c && _grid[i, 2] == c))
                {
                    hasWon = true;
                    break;
                }
                /*if ()
                {
                    hasWon = true;
                    break;
                }*/
            }

            if (_grid[0, 0] == c && _grid[1, 1] == c && _grid[2, 2] == c
             || _grid[0, 2] == c && _grid[1, 1] == c && _grid[2, 0] == c) hasWon = true;

            return hasWon;
        }

        private static void DrawBoard(char[,] grid, int rows, int columns)
        {
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    Console.Write(grid[i,j]);
                    if (j < 2) Console.Write('|');
                }
                Console.WriteLine();
                if (i < 2) Console.WriteLine("-----");
            }
        }
    }
}
