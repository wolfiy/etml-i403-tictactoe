﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ch.etml.TicTacToe.Utils
{
    internal class Checker
    {
        public static bool InputIsValid()
        {
            return true;
        }

        public static bool PositionCanBeUsed(bool[,] playedPositions, int row, int col)
        {
            return !playedPositions[row, col];
        }
    }
}
